FROM node:18.19.0-alpine3.19 As development
WORKDIR /cache
RUN npm i -g pnpm
COPY package.json pnpm-lock.yaml ./
RUN pnpm i --frozen-lockfile

FROM node:18.19.0-alpine3.19 as build
WORKDIR /cache
RUN npm i -g pnpm
COPY . .
COPY --from=development /cache/node_modules ./node_modules
RUN pnpm build

FROM node:18.19.0-alpine3.19 as production
WORKDIR /app
RUN npm i -g pnpm
COPY --from=build /cache/.output /app
EXPOSE 3000
CMD ["pnpm", "serve --host 0.0.0.0"]
