stages:
  - install
  - lint
  - build
  - deploy
  - clean

image: node:16.16.0-alpine

variables:
  NODE_OPTIONS: --max_old_space_size=4096

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cache/
    - node_modules/
    - .nuxt/
    - .output/

install_dependencies:
  stage: install
  script:
    - npm i -g pnpm
    - pnpm i --frozen-lockfile
  tags:
    - runner-local

.lint:
  stage: lint
  before_script:
    - npm i -g pnpm
    - pnpm i --frozen-lockfile
  tags:
    - runner-local

commit-msg-lint:
  extends:
    - .lint
  script:
    - pnpm type-prepare
    - echo "${CI_COMMIT_MESSAGE}" | npx commitlint

code-lint:
  extends:
    - .lint
  script:
    - pnpm type-prepare
    - pnpm lint:eslint && pnpm lint:format && pnpm lint:style

code-type-check:
  extends:
    - .lint
  script:
    - pnpm type-check

.build:
  stage: build
  image: docker:19
  services:
    - docker:dind
  before_script:
    - apk update
    - apk add zip
  script:
    - docker build -t gnha_website_ssr:1.0 -f dockerfile.ssr . --build-arg MODE=${CURRENT_MODE}
    - docker build -t gnha_website_csr:1.0 -f dockerfile.csr . --build-arg MODE=${CURRENT_MODE}
    - docker save -o gnha_website_ssr${CI_PIPELINE_ID}.tar gnha_website_ssr:1.0
    - docker save -o gnha_website_csr${CI_PIPELINE_ID}.tar gnha_website_csr:1.0
    - zip gnha_website_ssr${CI_PIPELINE_ID}.zip gnha_website_ssr${CI_PIPELINE_ID}.tar
    - zip gnha_website_csr${CI_PIPELINE_ID}.zip gnha_website_csr${CI_PIPELINE_ID}.tar
  artifacts:
    paths:
      - gnha_website_ssr${CI_PIPELINE_ID}.zip
      - gnha_website_csr${CI_PIPELINE_ID}.zip
  tags:
    - runner-local

build-other-branches:
  extends:
    - .build
  before_script:
    - !reference [.build, before_script]
    - export CURRENT_MODE=staging
    - echo $CURRENT_MODE
  except:
    - staging
    - production

build-staging:
  extends:
    - .build
  before_script:
    - !reference [.build, before_script]
    - export CURRENT_MODE=staging
    - echo $CURRENT_MODE
  only:
    - staging

build-production:
  extends:
    - .build
  before_script:
    - !reference [.build, before_script]
    - export CURRENT_MODE=production
    - echo $CURRENT_MODE
  only:
    - production

.deploy:
  stage: deploy
  before_script:
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - chmod 700 ~/.ssh
  script:
    - scp -P ${SSH_PORT} gnha_website_ssr${CI_PIPELINE_ID}.zip gnha_website_csr${CI_PIPELINE_ID}.zip ${SSH_USER}@${CURRENT_SSH_HOST}:/home/gnha-web-app
    - >-
      ssh -p ${SSH_PORT} ${SSH_USER}@${CURRENT_SSH_HOST}
      "cd /home/gnha-web-app
      && unzip gnha_website_ssr${CI_PIPELINE_ID}.zip
      && unzip gnha_website_csr${CI_PIPELINE_ID}.zip
      && cat gnha_website_ssr${CI_PIPELINE_ID}.tar | docker load
      && cat gnha_website_csr${CI_PIPELINE_ID}.tar | docker load
      && docker-compose up -d
      && docker-compose restart
      && docker image prune -a -f
      && rm gnha_website_ssr${CI_PIPELINE_ID}.* gnha_website_csr${CI_PIPELINE_ID}.*
      && exit"
  tags:
    - runner-local

deploy-staging:
  extends:
    - .deploy
  before_script:
    - !reference [.deploy, before_script]
    - export CURRENT_SSH_HOST=${SSH_HOST_STAG}
    - echo $CURRENT_SSH_HOST
  only:
    - staging

deploy-production:
  extends:
    - .deploy
  before_script:
    - !reference [.deploy, before_script]
    - export CURRENT_SSH_HOST=${SSH_HOST}
    - echo $CURRENT_SSH_HOST
  only:
    - production

clean:
  stage: clean
  before_script:
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - chmod 700 ~/.ssh
    - export CURRENT_SSH_HOST=192.168.18.7
    - echo $CURRENT_SSH_HOST
  script:
    - echo "Cleaning up..."
    - >-
      ssh -p ${SSH_PORT} ${SSH_USER}@${CURRENT_SSH_HOST}
      "docker system prune -a -f && exit"
  when: on_failure
  tags:
    - runner-local
